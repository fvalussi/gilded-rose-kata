package com.gildedrose

class ItemBuilder {
    private var name: String = ""
    private var sellIn: Int = 10
    private var quality: Int = 10

    fun named(name: String): ItemBuilder {
        this.name = name
        return this
    }

    fun withSellIn(sellIn: Int): ItemBuilder {
        this.sellIn = sellIn
        return this
    }

    fun withQuality(quality: Int): ItemBuilder {
        this.quality = quality
        return this
    }

    fun build(): Item {
        return Item(name, sellIn, quality)
    }

}

fun anItem(): ItemBuilder {
    return ItemBuilder()
}