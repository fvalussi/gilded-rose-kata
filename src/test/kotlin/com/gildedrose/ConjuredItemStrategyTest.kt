package com.gildedrose

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class ConjuredItemStrategyTest {
    private val strategy = ConjuredItemStrategy()

    @Test
    fun `should decrease item SellIn one time`() {
        val item = anItem().named("Conjured Mana Cake").withSellIn(0).build()

        strategy.update(item)

        assertEquals(-1, item.sellIn)
    }

    @Test
    fun `should decrease item Quality two times`() {
        val item = anItem().named("Conjured Mana Cake").withQuality(10).build()

        strategy.update(item)

        assertEquals(8, item.quality)
    }

    @Test
    fun `should decrease item Quality value four times after the sell by date`() {
        val item = anItem().named("Conjured Mana Cake").withQuality(10).withSellIn(0).build()

        strategy.update(item)

        assertEquals(6, item.quality)
    }

    @Test
    fun `should not decrease item Quality below 0`() {
        val item = anItem().named("Conjured Mana Cake").withQuality(0).build()

        strategy.update(item)

        assertEquals(0, item.quality)
    }
}