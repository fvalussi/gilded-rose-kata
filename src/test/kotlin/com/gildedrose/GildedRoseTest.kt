package com.gildedrose

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class GildedRoseTest {

    @Test
    fun `at the end of the day should lower the Quality and the SellIn of every item`() {
        val item = Item(NORMAL_ITEM_NAME, 1, 10)
        `given an item`(item)

        `when the day ends`()

        `the item sellIn is`(0)
        `the item quality is`(9)
    }

    @Test
    fun `once the sell by date has passed, Quality degrades twice as fast`() {
        val item = Item(NORMAL_ITEM_NAME, 0, 10)
        `given an item`(item)

        `when the day ends`()

        `the item sellIn is`(-1)
        `the item quality is`(8)
    }

    @Test
    fun `the Quality of an item is never negative`() {
        `given an item`(anItem().withSellIn(0).withQuality(0).build())

        `when the day ends`()

        `the item quality is`(0)
    }

    @Test
    fun `Aged Brie increases in Quality the older it gets`() {
        `given an item`(anItem().named(VINTAGE_ITEM_NAME).withSellIn(1).withQuality(0).build())

        `when the day ends`()

        `the item sellIn is`(0)
        `the item quality is`(1)
    }

    @Test
    fun `once the sell by date has passed, Aged Brie Quality increases twice as fast`() {
        `given an item`(anItem().named(VINTAGE_ITEM_NAME).withSellIn(0).withQuality(0).build())

        `when the day ends`()

        `the item quality is`(2)
    }

    @Test
    fun `the Quality of an Item is never more than 50`() {
        `given an item`(anItem().named(VINTAGE_ITEM_NAME).withQuality(50).build())

        `when the day ends`()

        `the item quality is`(50)
    }

    @Test
    fun `legendary items do not have to sell in nor decrease in Quality`() {
        `given an item`(anItem().named(LEGENDARY_ITEM_NAME).withSellIn(1).withQuality(1).build())

        `when the day ends`()

        `the item sellIn is`(1)
        `the item quality is`(1)
    }

    @Test
    fun `legendary items do not have to sell in nor decrease in Quality after the SellIn`() {
        `given an item`(anItem().named(LEGENDARY_ITEM_NAME).withSellIn(-1).withQuality(1).build())

        `when the day ends`()

        `the item sellIn is`(-1)
        `the item quality is`(1)
    }

    @Test
    fun `Backstage passes Quality increases as its SellIn approaches`() {
        `given an item`(anItem().named(BACKSTAGE_PASSES_NAME).withSellIn(100).withQuality(10).build())

        `when the day ends`()

        `the item quality is`(11)
    }

    @Test
    fun `Backstage passes Quality increases twice as fast when there are 10 days or less until the SellIn`() {
        `given an item`(anItem().named(BACKSTAGE_PASSES_NAME).withSellIn(10).withQuality(10).build())

        `when the day ends`()

        `the item quality is`(12)
    }

    @Test
    fun `Backstage passes Quality increases triple as fast when there are 5 days or less until the SellIn`() {
        `given an item`(anItem().named(BACKSTAGE_PASSES_NAME).withSellIn(5).withQuality(10).build())

        `when the day ends`()

        `the item quality is`(13)
    }

    @Test
    fun `Backstage passes Quality drops to 0 after the SellIn`() {
        `given an item`(anItem().named(BACKSTAGE_PASSES_NAME).withSellIn(0).withQuality(10).build())

        `when the day ends`()

        `the item quality is`(0)
    }

    private fun `given an item`(item: Item) {
        app = GildedRose(arrayOf(item))
    }

    private fun `when the day ends`() {
        app.updateQuality()
    }

    private fun `the item quality is`(quality: Int) {
        val itemQuality = app.items[0].quality
        assertEquals(quality, itemQuality)
    }

    private fun `the item sellIn is`(sellIn: Int) {
        val itemSellIn = app.items[0].sellIn
        assertEquals(sellIn, itemSellIn)
    }

    companion object {
        private lateinit var app: GildedRose
        private const val NORMAL_ITEM_NAME = "Elixir of the Mongoose"
        private const val LEGENDARY_ITEM_NAME = "Sulfuras, Hand of Ragnaros"
        private const val VINTAGE_ITEM_NAME = "Aged Brie"
        private const val BACKSTAGE_PASSES_NAME = "Backstage passes to a TAFKAL80ETC concert"
    }
}
