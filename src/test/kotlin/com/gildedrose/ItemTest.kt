package com.gildedrose

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ItemTest {
    @Test
    fun `should print its Name, Quality and SellIn`() {
        val name = "A NAME"
        val sellIn = 10
        val quality = 50
        val item = anItem().named(name).withSellIn(sellIn).withQuality(quality).build()

        assertEquals("$name, $sellIn, $quality", item.toString())
    }
}