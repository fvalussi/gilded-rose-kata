package com.gildedrose

class ConjuredItemStrategy : UpdateStrategy {
    override fun update(i: Item) {
        decreaseSellIn(i)
        decreaseQuality(i)
        decreaseQuality(i)
        if (i.sellIn <= 0) {
            decreaseQuality(i)
            decreaseQuality(i)
        }
    }

    private fun decreaseSellIn(item: Item) {
        --item.sellIn
    }

    private fun decreaseQuality(item: Item) {
        if (item.quality > 0) --item.quality
    }
}