package com.gildedrose

interface UpdateStrategy {
    fun update(i: Item)
}