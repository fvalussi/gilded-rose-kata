package com.gildedrose

class GildedRose(var items: Array<Item>) {
    private var updateStrategy: UpdateStrategy? = null

    fun updateQuality() {
        items.forEach { i ->

            updateStrategy = strategyFor(i)

            if (updateStrategy != null) {
                updateStrategy?.update(i)
            } else {
                when (i.name) {
                    AGED_BRIE -> {
                        increaseQuality(i)
                    }
                    BACKSTAGE_PASSES -> {
                        increaseQuality(i)
                        if (i.sellIn < 11) increaseQuality(i)
                        if (i.sellIn < 6) increaseQuality(i)
                    }
                    SULFURAS -> {
                        // nothing
                    }
                    else -> {
                        decreaseQuality(i)
                    }
                }

                if (i.name != SULFURAS) {
                    i.sellIn = i.sellIn - 1
                }

                if (i.sellIn < 0) {
                    when (i.name) {
                        AGED_BRIE -> {
                            increaseQuality(i)
                        }
                        BACKSTAGE_PASSES -> {
                            i.quality = 0
                        }
                        SULFURAS -> {
                            // nothing
                        }
                        else -> {
                            decreaseQuality(i)
                        }
                    }
                }
            }
        }
    }

    private fun decreaseQuality(i: Item) {
        if (i.quality > 0) {
            i.quality = i.quality - 1
        }
    }

    private fun increaseQuality(i: Item) {
        if (i.quality < 50) {
            i.quality = i.quality + 1
        }
    }

    companion object {
        private const val BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert"
        private const val AGED_BRIE = "Aged Brie"
        private const val SULFURAS = "Sulfuras, Hand of Ragnaros"

        private fun strategyFor(item: Item): UpdateStrategy? {
            return when {
                item.name.contains("Conjured") -> ConjuredItemStrategy()
                else -> null
            }
        }
    }
}